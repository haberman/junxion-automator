#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import argparse
import logging

import urllib.request

import re
import csv
import json
import os
import subprocess

PROG_NAME = 'junxion-automator'
PROG_VERSION = 0.6

POLYGON_API = 'http://polygons.openstreetmap.fr/get_poly.py'
BOUNDARY_FORMAT = 'poly'

COMMAND_DIR = '../_osmium/build'


def delete_folder(path):
    files = os.listdir(path)

    for f in files:
        f = os.path.join(path, f)
        if os.path.isdir(f):
            delete_folder(f)
            os.removedirs(f)
        else:
            os.remove(f)


def download_boundary(boundary, args):
    """
    Download the boundary polygon file.
    Calls the `POLYGON_API <http://polygons.openstreetmap.fr>`_ with the provided  `boundary` csv line.

    Args:
            * boundary: a csv row in the form of a list: `[city_name, boundary_osm_relation_id, administrative_level]`;
            * args: the `list` of arguments used to call the automator.

    Returns:
            An info dict with 2 keys: `city` and `admin_level` for easily locating the polygon file.
    """

    boundary_url = '{}?id={}&params=0'.format(POLYGON_API, boundary[1])
    boundary_folder = '{}{}'.format(args.o, boundary[0])
    boundary_base = '{}/boundary-{}'.format(boundary_folder, boundary[2])
    boundary_file = '{}.{}'.format(boundary_base, BOUNDARY_FORMAT)

    if args.d < 2 and os.path.isfile(boundary_file):
        return {'city': boundary[0], 'admin_level': boundary[2]}

    if not os.path.exists(boundary_folder):
        os.makedirs(boundary_folder)

    if args.verbose:
        print(
            '  -> {}: {}.{}'.format(boundary[1], boundary[0], BOUNDARY_FORMAT))

    with open(boundary_file, 'w') as f:
        request = urllib.request.urlopen(boundary_url)
        f.write(request.read().decode(request.headers.get_content_charset()))

    return {'city': boundary[0], 'admin_level': boundary[2]}


def write_extract_conf(places, args):
    """
    Writes the `conf.json` file.
    Given the list of `download_boundary()`, creates a configuration file used with ``osmium extract``.

    Args:
            * places: a `list` of objects describing where the boudaries' files are stored;
            * args: the `list` of arguments used to call the automator.

    Returns:
            The path of the configuration file.
    """

    conf = {'directory': '{}'.format(args.o), 'extracts': []}
    conf_file = '{}conf.json'.format(args.o)

    if args.d < 1 and os.path.isfile(conf_file):
        return conf_file

    for boundary in places:
        extract = {
            'output': '{}/extract-{}.pbf'.format(boundary['city'],
                                                 boundary['admin_level']),
            'polygon': {
                'file_name': '{}/boundary-{}.{}'.format(boundary['city'],
                                                        boundary['admin_level'],
                                                        BOUNDARY_FORMAT),
                'file_type': BOUNDARY_FORMAT
            }
        }

        conf['extracts'].append(extract)

    with open(conf_file, 'w') as f:
        json.dump(conf, f)

    return conf_file


def extract(conf_file, args):
    """
    Extracts the area of a given pbf file.
    Configures and calls the ``osmium extract`` command with the provided `conf_file`.

    Args:
            * conf_file: The path of the configuration file;
            * args: the `list` of arguments used to call the automator.
    """

    commands = ['osmium', 'extract', '--overwrite', '-c', conf_file, args.i]
    subprocess.run(commands)


def parse(city, admin_level, args):
    """
    Writes the `junxions.geojson` file.
    Configures and calls our own `jxn_parser` command in order to extract nodes being crossed by multiple ways.

    Args:
            * city: The city for which we extract highways from;
            * admin_level: The administrative level at which we're searching for features;
            * folder: The folder where to save the city's junxions;
            * args: the `list` of arguments used to call the automator.

    Returns:
            The path of the created geojson file.
    """

    if args.verbose:
        print('  -> parse {}'.format(city))

    input_file = '{}{}/extract-{}.pbf'.format(args.o, city, admin_level)

    if not os.path.isfile(input_file):
        logging.error('Required map data ({}) is missing!'.format(input_file))
        return

    geojson_file = '{}{}/junxions-{}.geojson'.format(args.o, city, admin_level)

    if args.d < 1 and os.path.isfile(geojson_file):
        return geojson_file

    commands = ['{}/jxn_parser'.format(COMMAND_DIR), '-o', geojson_file]
    if args.w is not None:
        commands.append('-w')
        commands.append(str(args.w))
    if args.a is not None:
        commands.append('-a')
        commands.append(str(args.a))
    if args.verbose:
        commands.append('-v')

    commands.append(input_file)
    subprocess.run(commands)

    return geojson_file


def draw(city, admin_level, args):
    """
    Computes different types of drawings.
    Configures and calls our own `jxn_drawer` backend by feeding it a returned `parse()` geojson file.
    Note that since the `jxn_drawer` uses a relative font, that command needs to be called from its directory. So we're using `cwd` with absolute paths.

    Args:
            * city: The city for which we exract from;
            * admin_level: The administrative level at which we're searching for features;
            * args: the `list` of arguments used to call the automator with options telling what type of drawing to make.

    Returns:
            A dic made of as many keys as drawings' types.
    """
    if args.verbose:
        print('  -> {}'.format(city))

    commands = ['./jxn_drawer']

    if args.o:
        output = os.path.abspath('{}{}/{}'.format(args.o, city, 'drawer'))

        commands.append('-o')
        commands.append(output)

    if args.w:
        commands.append('-w')
        commands.append(str(args.w))

    if args.verbose:
        commands.append('-v')

    drawing_cmds = []
    if args.m:
        drawing_cmds.append('M')
    if args.v:
        drawing_cmds.append('V')
    if args.k:
        drawing_cmds.append('K')
    if args.d:
        drawing_cmds.append('D')

    geojson = os.path.abspath('{}{}/junxions-{}.geojson'.format(args.o,
                                                                city,
                                                                admin_level))

    for drawing_cmd in drawing_cmds:
        final_cmd = list(commands)
        final_cmd.append('-{}'.format(''.join(drawing_cmd)))
        final_cmd.append(geojson)

        subprocess.run(final_cmd, cwd=COMMAND_DIR)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser(prog='./{}.py'.format(PROG_NAME),
                                     description='Python tool to automate tasks involved in map features\' extraction',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity.')
    parser.add_argument('-c', default='conf.csv',
                        help='a csv file containing ids of places (administrative places) upon which to automate commands.')
    parser.add_argument('-o', default='output/',
                        help='the path where to save outputs of commands.')
    parser.add_argument('-d', type=int, default=0, choices=[0, 1, 2],
                        help='level of cleaning where {}, {} and {}.'.format('0 means nothing',
                                                                             '1 is for deleting generated configurations files',
                                                                             '2 is for deleting every bytes the automator may have written'))

    subparsers = parser.add_subparsers(help='Automated actions')

    extract_subparser = subparsers.add_parser('extract',
                                              help='run the boundary extraction tool.')
    extract_subparser.add_argument('-i', default='../belgium-latest.osm.pbf',
                                   help='the pbf input file.')

    parse_subparser = subparsers.add_parser('parse',
                                            help='extract a geojson representation of crossroads.')
    parse_subparser.add_argument('-w', type=int, nargs='?',
                                 help='minimum amount of ways for a node to be considered as a junxion.')
    parse_subparser.add_argument('-a', type=int, nargs='?',
                                 help='number of adjacent nodes a junxion should embed along each one of its underlying ways.')
    parse_subparser.add_argument('-e', action='store_true',
                                 help='Also exports output geojson to explorer')

    draw_subparser = subparsers.add_parser('draw',
                                           help='automate diverse generative map drawings.')
    draw_subparser.add_argument('-w', type=int, nargs='?',
                                help='The target width of the image output in pixels')
    draw_subparser.add_argument('-m', action='store_true',
                                help='Draws a map projection.')
    draw_subparser.add_argument('-k', action='store_true',
                                help='Draws a quadtree partition.')
    draw_subparser.add_argument('-v', action='store_true',
                                help='Draws a Voronoi diagram.')
    draw_subparser.add_argument('-d', action='store_true',
                                help='Draws a Delaunay triangulation.')

    try:
        args = parser.parse_args()

        # Create output folder
        if not os.path.isdir(args.o):
            os.makedirs(args.o)

        # Generic rejections
        if not os.access(args.o, os.W_OK):
            logging.error('Output path {} is not writable.'.format(args.o))
            exit(0)

        if not os.path.isfile(args.c):
            logging.error('The required conf file does not exist.')
            exit(0)

        # Early cleanup request
        if args.d == 2:
            logging.info('Cleaning output folder: {}.'.format(args.o))
            delete_folder(args.o)

        # subargs wich are unique to each subcommand
        do_extract = 'i' in args
        do_parse = 'a' in args
        do_draw = 'm' in args

        if not (do_extract or do_parse or do_draw):
            if args.verbose:
                logging.info('No action have been taken...')

            exit(0)

        # Parse automation csv file
        places = []
        rows = []

        with open(args.c, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)

            if args.verbose:
                print('Reading configuration...')

            for row in csv_reader:
                if row[3] == 'on':
                    rows.append(row)
                    places.append(download_boundary(row, args))

        # Extract subparser
        if do_extract:
            if args.i[-4:] != '.pbf':
                logging.error('Input {} is not a pbf file.'.format(args.i))
                exit(0)

            pbf_input = args.i

            if args.verbose:
                print('Write extract configuration file...')

            conf_file = write_extract_conf(places, args)

            if args.verbose:
                print('Extract places from input {}...'.format(pbf_input))

            extract(conf_file, args)

        # Parse subparser
        if do_parse:
            if args.verbose:
                print('Parse junxions...')

            for row in rows:
                geojson_file = parse(row[0], row[2], args)
                if args.e:
                    if args.verbose:
                        print('Clone json to explorer...')

                    with open(geojson_file, 'r') as geojson:
                        compact_geojson = re.sub(r'(\s)+', '', geojson.read())

                        with open('../_explorer/junxions/{}-{}.geojson'.format(row[0], row[2]), 'w+') as explorer_export:
                            explorer_export.write(compact_geojson)
                            explorer_export.close()
                        geojson.close()

        # Draw subparser
        if do_draw:
            if args.verbose:
                print('Draw junxions...')

            for row in rows:
                draw(row[0], row[2], args)

    except IOError as msg:
        print('error')
        parser.error(str(msg))
