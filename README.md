## junXion automator 0.4
Python tool to automate tasks involved in map junxions' extraction, projection and drawing.
Works with an input .pbf file (from [geofabrik](http://download.geofabrik.de/) for instance) and a manually encoded list of boudaries' OSM ids that's finally taken to exract regions out of which we can parse junxions. The automation is made by using a simple `conf.csv` file listing the boundary OSM ids upon which sub pbf will be extracted, hence the automation.

### usage
```
usage: ./junxion-automator.py [-h] [-v] [-c C] [-o O] [-d {0,1,2}]
                              {extract,parse,draw} ...

positional arguments:
  {extract,parse,draw}  Automated actions
    extract             run the boundary extraction tool.
    parse               extract a geojson representation of crossroads.
    draw                automate diverse generative map drawings.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity. (default: False)
  -c C                  a csv file containing ids of boundaries
                        (administrative places) upon which to automate
                        commands. (default: conf.csv)
  -o O                  the path where to save outputs of commands. (default:
                        output/)
  -d {0,1,2}            level of cleaning where 0 means nothing, 1 is for
                        deleting generated configurations files and 2 is for
                        deleting every bytes the automator may have written.
                        (default: 0)
```

#### dependencies
- [Osmium Tool](https://osmcode.org/osmium-tool/)

### module documentation

```
download_boundary(boundary, args)
```
Download the boundary polygon file.
Calls the [POLYGON_API] (http://polygons.openstreetmap.fr) with the provided  `boundary` csv line.

Args:
* boundary: a csv row in the form of a list: `[city_name, boundary_osm_relation_id, administrative_level]`;
* args: the `list` of arguments used to call the automator.

Returns:
An info dict with 2 keys: `folder` and `admin_level`.
        
```
write_extract_conf(boundaries, args)
```
Writes the `conf.json` file.
Given the list of `download_boundary()`, creates a configuration file used with ``osmium extract``.

Args:
* boundaries: a `list` of objects describing where the boudaries' files are stored;
* args: the `list` of arguments used to call the automator.

Returns:
The path of the configuration file.

```
extract(conf_file, args)
```
Extracts the area of a given pbf file.
Configures and calls the ``osmium exract`` command with the provided `conf_file`.

Args:
* conf_file: The path of the configuration file;
* args: the `list` of arguments used to call the automator.

```
extract_junxions(city, admin_level, folder, args)
```
Writes the `junxions.geojson` file.
Configures and calls our own `junxion-parser` command in order to exract nodes that are crossed by multiple ways.

Args:
* city: The city for which we extract highways from;
* admin_level: The administrative level at which we're searching for features;
* folder: The folder where to save the city's junxions;
* args: the `list` of arguments used to call the automator.

Returns:
The path of the created geojson file.

```
filter_highway(city, admin_level, args)
```
Filters a new pbf extract made only of  `highway` tagged entites.
Configures and calls the ``osmium tags-filter`` with the provided method arguments.

Args:
* city: The city for which we extract highways from;
* admin_level: The administrative level at which we're searching for features;
* args: the `list` of arguments used to call the automator.

Returns:
The path of the output (filtered) pbf file.

## Licence
[WTFPL](http://www.wtfpl.net/) – Do What the Fuck You Want to Public License.
